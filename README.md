# twitbase

a quick app to find keybase users among your followers

## setup

First,

    npm install

Then, create `config.json` (get the keys from https://apps.twitter.com/):

    {
      "consumer_key": "",
      "consumer_secret": "",
      "access_token_key": "",
      "access_token_secret": ""
    }

Finally, you are ready to run:

    ulimit -S -n 4096
    node index
