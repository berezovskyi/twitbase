"use strict"
var async = require("async");
var Twitter = require('twitter');
var kbuser = require('keybase-user');
var nopt = require("nopt")
  , knownOpts = {
    "username" : [String],
    "debug" : [Boolean],
    "skip" : [Number],
    "concurrency" : [Number],
  }
  , flags = {
    "username": ["--username", "berezovskiy"],
    "debug": ["--debug", false],
    "skip": ["--skip", 0],
    "concurrency": ["--concurrency", 10],  //TODO deprecated
  }
  , opt = nopt(knownOpts, flags)
var config = require('./config.json');

var LIMIT = 200;

var client = new Twitter({
  consumer_key: config.consumer_key,
  consumer_secret: config.consumer_secret,
  access_token_key: config.access_token_key,
  access_token_secret: config.access_token_secret
});

var params = {
  screen_name: opt.username,
  count: LIMIT
};

process.on('unhandledRejection', function(reason, p){
    // console.log("Possibly Unhandled Rejection at: Promise ", p, " reason: ", reason);
    // TODO handle zombie processes
});

client.get('followers/list', params, function(error, followers, response){
  if (!error) {
    var follower_usernames = followers.users.map(function (f) {
      return f.screen_name
    })
    if(follower_usernames.length == LIMIT) {
      console.log("WARNING: pagination is necessary");
    }
    var check_names = follower_usernames.slice(opt.skip)
    if(opt.debug) {
      console.log(check_names);
    }
    async.eachSeries(check_names, function (uname, callback) {
      process.stdout.write('.');
      kbuser(uname).then(function (kb) {
        if(kb) {
          process.stdout.write('\n');
          console.log(uname);
        }
        callback();
      });
    });
  } else {
    console.log(error);
  }
});
